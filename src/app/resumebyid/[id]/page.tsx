"use client";
import React, { useState, useEffect } from "react";
import { useParams } from "next/navigation";
import axios from "axios";
interface IResumeData {
  _id: string;
  name: string;
  position: string;
  address: string;
  email: string;
  education: string;
  skills: string;
  experience: string;
  professionalSummary: string;
  projects: [{ name: ""; technology: ""; role: "" }];
}
const resumebyid = () => {
  const { id } = useParams();
  // console.log("id----", id);

  const [resumeData, setResumeData] = useState<IResumeData[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      if (id) {
        try {
          const response = await axios.get(
            `https://share-resume.onrender.com/${id}`
          );
          setResumeData(response.data.data);
        } catch (error) {
          console.error(error);
        }
      } else {
        console.log("Resume not found");
      }
    };

    fetchData();
  }, [id]);

  return (
    <React.Fragment>
      {resumeData.map((value, index) => (
        <div
          key={`resumeData+ ${index}`}
          className="py-4 px-16 bg-slate-50 h-screen"
        >
          <h1 className="text-4xl font-bold text-blue-500">{value.name}</h1>
          <h1 className="text-md text-black mt-2 font-bold">
            {value.position}
          </h1>
          <h1 className="text-md text-black mt-2 font-bold">{value.email}</h1>
          <h1 className="text-md text-black mt-2 font-bold">{value.address}</h1>

          {/* for professional summary whole div */}
          <div>
            <div className="text-xl bg-blue-400 text-white flex justify-center mt-4">
              <h2>Professional Summary</h2>
            </div>
            <div className="text-black text-lg mt-4">
              <p>{value.professionalSummary}</p>
            </div>
          </div>
          {/* completed professional summary div */}

          {/* div for skill set */}
          <div className="mt-2">
            <div className="text-xl bg-blue-400 text-white flex justify-center mt-4">
              <h2>Skills set</h2>
            </div>
            <div className="text-black  mt-4">
              <h2>Technology : {value.skills}</h2>
            </div>
          </div>
          {/* completed div of skill set */}

          {/* Experience div started */}
          <div className="mt-2">
            <div className="text-xl bg-blue-400 text-white flex justify-center mt-4">
              <h2>Work Experience</h2>
            </div>
            <div className="text-black mt-4">
              <h2>Work Experience : {value.experience}</h2>
            </div>
          </div>
          {/* Experience div ended */}

          {/* Eduction  div started */}
          <div className="mt-2">
            <div className="text-xl bg-blue-400 text-white flex justify-center mt-4">
              <h2>Education</h2>
            </div>
            <div className="text-black mt-4">
              <h2>{value.education}</h2>
            </div>
          </div>
          {/* Eduction  div ended */}

          {/* projects  div started */}
          <div className="mt-2">
            <div className="text-xl bg-blue-400 text-white flex justify-center mt-4">
              <h2>Projects</h2>
            </div>
            <div className="text-black mt-4">
              {value.projects.map((value, index) => (
                <div className="mb-5" key={`projects+${index}`}>
                  <h2>Name : {value.name}</h2>
                  <h2>Technology : {value.technology}</h2>
                  <h2>Role : {value.role}</h2>
                </div>
              ))}
            </div>
          </div>
          {/* projects  div ended */}
        </div>
      ))}
    </React.Fragment>
  );
};

export default resumebyid;
