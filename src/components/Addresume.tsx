"use client";
import axios from "axios";
import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const initialFormData = {
  name: "",
  skills: "",
  email: "",
  education: "",
  experience: "",
  professionalSummary: "",
  position: "",
  projects: [
    {
      name: "",
      technology: "",
      role: "",
    },
  ],
  address: "",
};

const Addresume = () => {
  const [formData, setFormData] = useState(initialFormData);

  //handle form change event & setting form data values
  const handleFormChange = (e: any) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  //resetting form function
  const resetForm = () => {
    setFormData(initialFormData);
  };

  //handle add textfields of projects
  const handleAddProject = () => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      projects: [
        ...prevFormData.projects,
        {
          name: "",
          technology: "",
          role: "",
        },
      ],
    }));
  };

  //handle Delete textfields of projects

  const handleDeleteProject = (index: number) => {
    setFormData((prevFormData) => {
      const projects = [...prevFormData.projects];

      if (projects.length > 1) {
        projects.splice(index, 1);
      }

      return { ...prevFormData, projects };
    });
  };

  //handle textfields of project change event
  const handleProjectFieldChange = (
    index: number,
    fieldName: string,
    value: string
  ) => {
    setFormData((prevFormData) => {
      const projects = [...prevFormData.projects];
      projects[index] = { ...projects[index], [fieldName]: value };
      return { ...prevFormData, projects };
    });
  };

  //form submit
  const handleSubmit = async (e: any) => {
    e.preventDefault();

    // if we want to print data to console
    // console.log("Form Data:", { ...formData });

    //Checking validation

    const isFormValid = validateForm();
    if (!isFormValid) {
      return;
    }

    // API Call
    try {
      await axios
        .post("https://share-resume.onrender.com/add", { ...formData })
        .then((response) => {
          console.log("API response:", response.data);
        });
    } catch (error) {
      console.error("API error:", error);
    }

    //resetting data
    resetForm();
  };

  //validation function
  const validateForm = () => {
    if (formData.name.trim() === "") {
      toast.error("Name Can not be empty ");
      return false;
    } else if (formData.name.length > 20) {
      toast.error("Name length should be less than 20");
      return false;
    } else if (formData.skills.trim() === "") {
      toast.error("Skills is required field");
      return false;
    } else if (formData.experience.trim() === "") {
      toast.error("Experience is required field");
      return false;
    } else if (formData.education.trim() === "") {
      toast.error("Education is required field");
      return false;
    } else if (formData.email.trim() === "") {
      toast.error("Email is required field");
      return false;
    } else if (
      formData.professionalSummary.trim() === "" ||
      formData.professionalSummary.length > 80
    ) {
      toast.error(
        "Professional Summary is required field and it can not be grater than 80"
      );
      return false;
    }
    return true;
  };

  return (
    <React.Fragment>
      {/* Toastify container */}
      <ToastContainer />

      <div className="flex justify-center">
        <h1 className="text-3xl font-bold font-mono ">Share Resume</h1>
      </div>

      <form onSubmit={handleSubmit}>
        <div className="grid gap-6 mb-6 md:grid-cols-2 m-10">
          <div>
            <label
              htmlFor="name"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Name
            </label>
            <input
              name="name"
              type="text"
              id="name"
              key={"name"}
              className="bg-white border border-gray-300 text-black text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
              placeholder="Enter Full Name"
              onChange={handleFormChange}
              value={formData.name}
            />
          </div>
          <div>
            <label
              htmlFor="skills"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Skills
            </label>
            <input
              name="skills"
              type="text"
              id="skills"
              key={"skills"}
              className="bg-white border border-gray-300 text-black text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              placeholder="Skills"
              onChange={handleFormChange}
              value={formData.skills}
            />
          </div>
          <div>
            <label
              htmlFor="email"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Email address
            </label>
            <input
              name="email"
              type="email"
              id="email"
              key={"email"}
              className="bg-white border border-gray-300 text-black text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              placeholder="test@team.com"
              onChange={handleFormChange}
              value={formData.email}
            />
          </div>
          <div>
            <label
              htmlFor="education"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Education
            </label>
            <input
              name="education"
              type="text"
              id="education"
              key={"education"}
              className="bg-white border border-gray-300 text-black text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              placeholder="Education"
              onChange={handleFormChange}
              value={formData.education}
            />
          </div>
          <div>
            <label
              htmlFor="experience"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Experience
            </label>
            <input
              name="experience"
              type="text"
              id="experience"
              key={"experience"}
              className="bg-white border border-gray-300 text-black text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              placeholder="Experience"
              onChange={handleFormChange}
              value={formData.experience}
            />
          </div>
          <div>
            <label
              htmlFor="position"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Position
            </label>
            <input
              name="position"
              type="position"
              id="position"
              key={"position"}
              className="bg-white border border-gray-300 text-black text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              placeholder="Enter your position"
              onChange={handleFormChange}
              value={formData.position}
            />
          </div>
          <div>
            <label
              htmlFor="address"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Address
            </label>
            <input
              name="address"
              type="address"
              id="address"
              key={"address"}
              className="bg-white border border-gray-300 text-black text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
              placeholder="Enter Address"
              onChange={handleFormChange}
              value={formData.address}
            />
          </div>
          <div className="mb-6">
            <label
              htmlFor="professionalSummary"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Professional Summary
            </label>
            <textarea
              name="professionalSummary"
              id="professionalSummary"
              key={"professionalSummary"}
              className="bg-white border border-gray-300 text-black text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-8/12 p-2.5"
              placeholder="Write Professional Summary here..."
              onChange={handleFormChange}
              value={formData.professionalSummary}
            />
          </div>

          <div>
            <label
              htmlFor="projects"
              className="block mb-2 text-sm font-medium text-black ml-1"
            >
              Projects
            </label>
            <button
              id="add"
              key="add"
              type="button"
              onClick={handleAddProject}
              className="text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center items-start"
            >
              ADD
            </button>
            {formData.projects.map((project, index) => (
              <div key={index}>
                <div className="flex">
                  <input
                    id="projectName"
                    key="projectName"
                    type="text"
                    className="border rounded p-2 mt-4 ml-2"
                    value={project.name}
                    onChange={(e) =>
                      handleProjectFieldChange(index, "name", e.target.value)
                    }
                    placeholder="Project Name"
                  />
                  <input
                    id="projectTechnology"
                    key="projectTechnology"
                    type="text"
                    className="border rounded p-2 mt-4 ml-2"
                    value={project.technology}
                    onChange={(e) =>
                      handleProjectFieldChange(
                        index,
                        "technology",
                        e.target.value
                      )
                    }
                    placeholder="Technology"
                  />
                  <input
                    id="projectRole"
                    key="projectRole"
                    type="text"
                    className="border rounded p-2 mt-4 ml-2"
                    value={project.role}
                    onChange={(e) =>
                      handleProjectFieldChange(index, "role", e.target.value)
                    }
                    placeholder="Role"
                  />
                  <button
                    id="delete"
                    key="delete"
                    type="button"
                    onClick={() => handleDeleteProject(index)}
                    className=" mt-4 ml-2 text-white bg-red-700 hover:bg-red-900 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
                  >
                    Delete
                  </button>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="flex justify-center">
          <button
            id="submit"
            key="submit"
            type="submit"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
          >
            Submit
          </button>
        </div>
      </form>
    </React.Fragment>
  );
};

export default Addresume;
