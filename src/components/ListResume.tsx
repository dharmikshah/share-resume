"use client";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import Link from "next/link";

interface IResumeData {
  _id: string;
  name: string;
  position: string;
  education: string;
  skills: string;
  experience: string;
  professionalSummary: string;
  email: string;
}
const ListResume = () => {
  const [resumeData, setResumeData] = useState<IResumeData[]>([]);
  const router = useRouter();
  useEffect(() => {
    axios
      .get("https://share-resume.onrender.com/resumes")
      .then((response) => {
        setResumeData(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);


  return (
    <React.Fragment>
      <div className="flex justify-center">
        <h1 className="text-3xl font-bold font-mono ">Resume List</h1>
      </div>
      <div className="mt-24">
        <table className="table-auto  shadow-lg bg-white mx-auto">
          <thead>
            <tr>
              <th className="bg-blue-100 border text-left px-8 py-4">Name</th>
              <th className="bg-blue-100 border text-left px-8 py-4">
                Position
              </th>
              <th className="bg-blue-100 border text-left px-8 py-4">Email</th>
              <th className="bg-blue-100 border text-left px-8 py-4">
                Education
              </th>
              <th className="bg-blue-100 border text-left px-8 py-4">Skills</th>
              <th className="bg-blue-100 border text-left px-8 py-4">
                Experience
              </th>

              <th className="bg-blue-100 border text-left px-8 py-4">
                Professional Summary
              </th>

              <th className="bg-blue-100 border text-left px-8 py-4">
                View Resume
              </th>
            </tr>
          </thead>
          <tbody>
            {resumeData.map((data, index) => (
              <tr key={`resumeList` + index}>
                <td className="border px-8 py-4">{data.name}</td>
                <td className="border px-8 py-4">{data.position}</td>
                <td className="border px-8 py-4">{data.email}</td>
                <td className="border px-8 py-4">{data.education}</td>
                <td className="border px-8 py-4">{data.skills}</td>
                <td className="border px-8 py-4">{data.experience}</td>
                <td className="border px-8 py-4">{data.professionalSummary}</td>
                <td className="border px-8 py-4">
                  <Link href={`./resumebyid/${data._id}`}>
                    <button className="text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center items-start">
                      View
                    </button>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
};

export default ListResume;
